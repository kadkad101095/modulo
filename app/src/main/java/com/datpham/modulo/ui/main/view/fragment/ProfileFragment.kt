package com.datpham.modulo.ui.main.view.fragment

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import com.datpham.modulo.R
import com.datpham.modulo.data.model.Address
import com.datpham.modulo.data.model.User
import com.datpham.modulo.databinding.FragmentProfileBinding
import com.datpham.modulo.ui.base.BaseFragment
import com.datpham.modulo.ui.main.viewmodel.MainViewModel
import com.datpham.modulo.ui.main.viewmodel.ProfileViewModel
import com.datpham.modulo.utils.formatDate
import com.datpham.modulo.utils.isValidDate
import com.datpham.modulo.utils.showToast
import com.datpham.modulo.utils.toDate

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {

    private val shareViewModel: MainViewModel by activityViewModels()

    val avatar = MutableLiveData<String>()
    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val birthDay = MutableLiveData<String>()
    val city = MutableLiveData<String>()
    val postalCode = MutableLiveData<String>()
    val street = MutableLiveData<String>()
    val streetCode = MutableLiveData<String>()
    val country = MutableLiveData<String>()

    override fun getLayout(): Int {
        return R.layout.fragment_profile
    }

    override fun getViewModel(): Class<ProfileViewModel> {
        return ProfileViewModel::class.java
    }

    override fun setupUI() {
        mBinding.handler = this
    }

    override fun setupObserver() {
        shareViewModel.userData.observe(viewLifecycleOwner) {
            initUser(it)
        }
    }

    fun initUser(user: User) {
        avatar.postValue(user.getDefaultAvatar())
        firstName.postValue(user.firstName)
        lastName.postValue(user.lastName)
        birthDay.postValue(user.birthDate.formatDate())
        city.postValue(user.address.city)
        postalCode.postValue("${user.address.postalCode}")
        street.postValue(user.address.street)
        streetCode.postValue(user.address.streetCode)
        country.postValue(user.address.country)
    }

    fun onCloseClick(view: View) {
        requireActivity().onBackPressed()
    }

    fun onConfirmClick(view: View) {
        //Check null or empty
        arrayOf(
            avatar.value,
            firstName.value,
            lastName.value,
            birthDay.value,
            city.value,
            postalCode.value,
            street.value,
            streetCode.value,
            country.value
        ).forEach {
            if (it.isNullOrBlank()) {
                showToast(R.string.please_fill_all_information)
                return
            }
        }
        //Check valid birthday format
        if (birthDay.value?.isValidDate("dd/MM/yyyy") == false) {
            showToast(R.string.invalid_birthday)
            return
        }
        //All information is ready now
        val birthdayMilisecond = birthDay.value!!.toDate("dd/MM/yyyy")!!.time
        val address =
            Address(
                city.value!!,
                postalCode.value!!.toInt(),
                street.value!!,
                streetCode.value!!,
                country.value!!
            )
        val newUserInfo = User(firstName.value!!, lastName.value!!, address, birthdayMilisecond)
        shareViewModel.userData.postValue(newUserInfo)
        requireActivity().onBackPressed()
    }

}