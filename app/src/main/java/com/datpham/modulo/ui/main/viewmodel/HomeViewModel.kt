package com.datpham.modulo.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.datpham.modulo.data.api.ApiManager
import com.datpham.modulo.data.model.Device
import com.datpham.modulo.data.model.User
import com.datpham.modulo.data.repository.local.AppDatabase
import com.datpham.modulo.data.repository.remote.response.SumUpResponse
import com.datpham.modulo.utils.DataStoreUtil
import com.datpham.modulo.utils.Resource
import com.datpham.modulo.utils.SingleLiveEvent
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var appDatabase: AppDatabase

    @Inject
    lateinit var apiManager: ApiManager

    val apiStatus = SingleLiveEvent<Resource<Any>>()
    val userData = SingleLiveEvent<User>()
    val listDevices = SingleLiveEvent<List<Device>>()
    val listDevicesSavedState = SingleLiveEvent<List<Device>>()

    //Get list devices and user information from backend
    fun getData() {
        apiManager.getData { resource ->
            apiStatus.postValue(resource)
            resource.data?.user?.let { user ->
                userData.postValue(user)
            }
            resource.data?.devices?.let { devices ->
                listDevices.postValue(devices)
            }
        }
    }

    //Reload local state of device when user interacted before
    fun reloadDeviceLocalState() {
        appDatabase.getAllDevices {
            listDevicesSavedState.postValue(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        apiManager.clear()
    }

}