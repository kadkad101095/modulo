package com.datpham.modulo.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.datpham.modulo.data.model.Device
import com.datpham.modulo.data.model.User

class MainViewModel : ViewModel() {
    val userData = MutableLiveData<User>()
    val deviceData = MutableLiveData<Device>()
}