package com.datpham.modulo.ui.main.view.dialog

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.datpham.modulo.R
import com.datpham.modulo.ui.customviews.MyButton

class NotyDialog(context: Context, message: String?, isSucess: Boolean = false) {
    init {
        with(Dialog(context)) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawableResource(R.color.transparent)
            setContentView(R.layout.dialog_noty)
            val tvContent = findViewById<TextView>(R.id.tvContent)
            tvContent.text = message
            val btnClose = findViewById<MyButton>(R.id.btnClose)
            btnClose.setOnClickListener { cancel() }
            val imgNotify = findViewById<ImageView>(R.id.imgNotify)
            imgNotify.setImageResource(if (isSucess) R.drawable.ic_success else R.drawable.ic_error)
            this
        }.also { dialog ->
            dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }.show()
    }
}