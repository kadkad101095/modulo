package com.datpham.modulo.ui.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.datpham.modulo.R

class MyButton : AppCompatButton {

    constructor(context: Context) : super(context) {
        setFont()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setFont()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        setFont()
    }

    private fun setFont() {
        if (this.typeface != null) {
            when (this.typeface.style) {
                Typeface.NORMAL -> {
                    val normal = Typeface.createFromAsset(
                        context.assets,
                        context.getString(R.string.font_regular)
                    )
                    setTypeface(normal, Typeface.NORMAL)
                }
                Typeface.BOLD -> {
                    val bold = Typeface.createFromAsset(
                        context.assets,
                        context.getString(R.string.font_bold)
                    )
                    setTypeface(bold, Typeface.BOLD)
                }
                Typeface.ITALIC -> {
                    val italic = Typeface.createFromAsset(
                        context.assets,
                        context.getString(R.string.font_italic)
                    )
                    setTypeface(italic, Typeface.ITALIC)
                }
                Typeface.BOLD_ITALIC -> {
                    val bold_italic = Typeface.createFromAsset(
                        context.assets,
                        context.getString(R.string.font_bold_italic)
                    )
                    setTypeface(bold_italic, Typeface.BOLD_ITALIC)
                }
            }
        } else {
            val normal = Typeface.createFromAsset(
                context.assets,
                context.getString(R.string.font_regular)
            )
            setTypeface(normal, Typeface.NORMAL)
        }
    }
}