package com.datpham.modulo.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class BaseFragment<VIEWDATABINDING : ViewDataBinding, VIEWMODEL : ViewModel> :
    Fragment() {

    lateinit var mBinding: VIEWDATABINDING
    lateinit var mViewModel: VIEWMODEL

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mViewModel = ViewModelProvider(this).get(getViewModel())
        return if (::mBinding.isInitialized) {
            mBinding.root
        } else {
            mBinding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
            mBinding.lifecycleOwner = viewLifecycleOwner
            with(mBinding) {
                root
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupObserver()
    }

    override fun onDestroy() {
        super.onDestroy()
        mBinding.unbind()
    }

    protected abstract fun getLayout(): Int

    protected abstract fun getViewModel(): Class<VIEWMODEL>

    protected abstract fun setupUI()

    protected abstract fun setupObserver()

}