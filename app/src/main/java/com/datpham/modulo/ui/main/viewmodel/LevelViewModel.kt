package com.datpham.modulo.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import com.datpham.modulo.data.model.Device
import com.datpham.modulo.data.repository.local.AppDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LevelViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var appDatabase: AppDatabase

    fun insertDevice(device: Device) {
        appDatabase.insertDevice(device) {}
    }

}