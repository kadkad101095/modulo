package com.datpham.modulo.ui.main.view.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import com.datpham.modulo.R
import com.datpham.modulo.ui.customviews.MyTextView
import java.lang.ref.WeakReference

class LoadingDialog(context: Activity) {
    private var mDialog: Dialog? = null
    var mContext: WeakReference<Activity> = WeakReference(context)

    init {
        mContext.get()?.let {
            with(Dialog(it, R.style.base_progess)) {
                setContentView(R.layout.dialog_loading)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCancelable(true)
                setCanceledOnTouchOutside(true)
                mDialog = this
            }
        }
    }

    fun show() {
        if (mDialog?.isShowing == false) {
            mDialog?.show()
        }
    }

    fun dismiss() {
        if (mDialog?.isShowing == true) {
            mDialog?.dismiss()
        }
    }
}