package com.datpham.modulo.ui.main.view.fragment

import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.datpham.modulo.R
import com.datpham.modulo.data.model.*
import com.datpham.modulo.databinding.FragmentLevelBinding
import com.datpham.modulo.ui.base.BaseFragment
import com.datpham.modulo.ui.main.viewmodel.LevelViewModel
import com.datpham.modulo.ui.main.viewmodel.MainViewModel
import com.datpham.modulo.utils.HEATER_MAX_TEMP
import com.datpham.modulo.utils.HEATER_MIN_TEMP
import com.datpham.modulo.utils.HEATER_STEP
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LevelFragment : BaseFragment<FragmentLevelBinding, LevelViewModel>() {

    private val shareViewModel: MainViewModel by activityViewModels()

    val name = MutableLiveData<String>()
    val iconType = MutableLiveData(R.mipmap.ic_light)
    val background = MutableLiveData(R.mipmap.bg_shadow)
    val isModeON = MutableLiveData(false)
    val level = MutableLiveData(0)
    val levelText = MutableLiveData<String>()
    val maxLevel = MutableLiveData(100)
    val isShowSwitch = MutableLiveData(true)

    var currentDevice: Device? = null

    override fun getLayout(): Int {
        return R.layout.fragment_level
    }

    override fun getViewModel(): Class<LevelViewModel> {
        return LevelViewModel::class.java
    }

    override fun setupUI() {
        mBinding.handler = this
    }

    override fun setupObserver() {

        shareViewModel.deviceData.observe(viewLifecycleOwner) {
            initDevice(it)
        }

        level.observe(viewLifecycleOwner) {
            if (currentDevice is Heater) {
                val newTempValue = "${HEATER_MIN_TEMP + (HEATER_STEP * it)}°C"
                levelText.postValue(newTempValue)
            } else {
                levelText.postValue("$it%")
            }
        }

    }

    fun initDevice(device: Device) {
        currentDevice = device
        name.postValue(device.deviceName)
        when (device) {
            is Light -> {
                iconType.postValue(R.mipmap.ic_light)
                background.postValue(R.mipmap.bg_light)
                level.postValue(device.intensity)
                isModeON.postValue(device.mode == DeviceMode.ON.type)
            }
            is Heater -> {
                iconType.postValue(R.mipmap.ic_heater)
                background.postValue(R.mipmap.bg_heater)
                val maxHeaterLevel = (HEATER_MAX_TEMP - HEATER_MIN_TEMP) / HEATER_STEP
                maxLevel.postValue(maxHeaterLevel.toInt())
                level.postValue(device.temperature)
                isModeON.postValue(device.mode == DeviceMode.ON.type)
            }
            is RollerShutter -> {
                iconType.postValue(R.mipmap.ic_rollershutter)
                background.postValue(R.mipmap.bg_rollershutter)
                level.postValue(device.position)
                isShowSwitch.postValue(false)
            }
        }
    }

    val swipeOnOffListener = object : MotionLayout.TransitionListener {
        override fun onTransitionStarted(layout: MotionLayout?, p1: Int, p2: Int) {

        }

        override fun onTransitionChange(layout: MotionLayout?, p1: Int, p2: Int, p3: Float) {

        }

        override fun onTransitionCompleted(layout: MotionLayout, p1: Int) {
            isModeON.postValue(layout.currentState == R.id.end)
        }

        override fun onTransitionTrigger(layout: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {

        }
    }

    fun onBackClick(view: View) {
        requireActivity().onBackPressed()
    }

    override fun onPause() {
        super.onPause()
        //Save device state when exit screen
        when (val currentDevice = currentDevice) {
            is Light -> {
                currentDevice.intensity = level.value ?: 0
                currentDevice.mode =
                    if (isModeON.value == true) DeviceMode.ON.type else DeviceMode.OFF.type
                mViewModel.insertDevice(currentDevice)
            }
            is Heater -> {
                currentDevice.temperature = level.value ?: 0
                currentDevice.mode =
                    if (isModeON.value == true) DeviceMode.ON.type else DeviceMode.OFF.type
                mViewModel.insertDevice(currentDevice)
            }
            is RollerShutter -> {
                currentDevice.position = level.value ?: 0
                mViewModel.insertDevice(currentDevice)
            }
        }
    }

}