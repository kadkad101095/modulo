package com.datpham.modulo.ui.main.view.fragment

import android.content.res.Configuration
import android.content.res.Resources
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.forEach
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import com.datpham.modulo.R
import com.datpham.modulo.data.model.*
import com.datpham.modulo.data.repository.local.AppDatabase
import com.datpham.modulo.databinding.FragmentHomeBinding
import com.datpham.modulo.navigator.AppNavigator
import com.datpham.modulo.navigator.Screens
import com.datpham.modulo.ui.base.BaseFragment
import com.datpham.modulo.ui.base.CustomFilter
import com.datpham.modulo.ui.base.RecyclerViewBuilder
import com.datpham.modulo.ui.main.adapter.DeviceAdapterObservable
import com.datpham.modulo.ui.main.view.dialog.ConfirmDialog
import com.datpham.modulo.ui.main.viewmodel.HomeViewModel
import com.datpham.modulo.ui.main.viewmodel.MainViewModel
import com.datpham.modulo.utils.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.runBlocking
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(),
    DeviceAdapterObservable.Callback {

    private val shareViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var dataStoreUtil: DataStoreUtil

    @Inject
    lateinit var appDatabase: AppDatabase

    @Inject
    lateinit var navigator: AppNavigator

    val userProfile = MutableLiveData<User>()
    private val listDevices = ArrayList<Device>()
    private val listFilterCondition = ArrayList<String>() //List used to save filter condition
    private var popupFilter: PopupMenu? = null

    override fun getLayout(): Int {
        return R.layout.fragment_home
    }

    override fun getViewModel(): Class<HomeViewModel> {
        return HomeViewModel::class.java
    }

    override fun setupUI() {
        mBinding.handler = this
        mBinding.adapter = RecyclerViewBuilder()
            .addRow(R.layout.item_home, DeviceAdapterObservable(this))
            .build(listDevices)
        if (listDevices.isEmpty()) {
            mViewModel.getData()
        } else {
            mViewModel.reloadDeviceLocalState()
        }
        initFilterList()
        initFilterCondition()
    }

    override fun setupObserver() {

        shareViewModel.userData.observe(viewLifecycleOwner) {
            userProfile.postValue(it)
        }

        mViewModel.apiStatus.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> showLoading()
                else -> hideLoading()
            }
        }

        mViewModel.userData.observe(viewLifecycleOwner) {
            userProfile.postValue(it)
        }

        mViewModel.listDevices.observe(viewLifecycleOwner) {
            listDevices.clear()
            listDevices.addAll(it)
            mBinding.adapter?.notifyDataSetChanged()
            mViewModel.reloadDeviceLocalState()
        }

        mViewModel.listDevicesSavedState.observe(viewLifecycleOwner) {
            val stateListIndex = it.groupBy {
                it.id
            }
            listDevices.forEach { device ->
                when (val state = stateListIndex.get(device.id)?.firstOrNull()) {
                    is Light -> {
                        device as Light
                        device.mode = state.mode
                        device.intensity = state.intensity
                    }
                    is Heater -> {
                        device as Heater
                        device.temperature = state.temperature
                        device.mode = state.mode
                    }
                    is RollerShutter -> {
                        device as RollerShutter
                        device.position = state.position
                    }
                }
            }
            mBinding.adapter?.notifyDataSetChanged()
            mBinding.adapter?.filter?.filter(null)
        }
    }

    private fun initFilterList() {
        listFilterCondition.clear()
        if (popupFilter == null) {
            //Default is show all devices
            listFilterCondition.add(DeviceType.LIGHT.type)
            listFilterCondition.add(DeviceType.HEATER.type)
            listFilterCondition.add(DeviceType.ROLLER.type)
        } else {
            //If popup existed -> filter condition existed, keep its state
            popupFilter?.menu?.forEach { item ->
                when (item.itemId) {
                    R.id.filter_light -> editFilterCondition(item.isChecked, DeviceType.LIGHT)
                    R.id.filter_heater -> editFilterCondition(item.isChecked, DeviceType.HEATER)
                    R.id.filter_roller -> editFilterCondition(item.isChecked, DeviceType.ROLLER)
                }
            }
        }
    }


    //Add condition to list devices adapter filter
    private fun initFilterCondition() {
        mBinding.adapter?.customFilter = object : CustomFilter {
            override fun getFilterdList(): List<Any> {
                return listDevices.filter {
                    return@filter listFilterCondition.contains(it.productType)
                }
            }
        }
    }

    private fun editFilterCondition(isSelected: Boolean, deviceType: DeviceType) {
        if (isSelected) {
            if (deviceType.type !in listFilterCondition) {
                listFilterCondition.add(deviceType.type)
            }
        } else {
            listFilterCondition.remove(deviceType.type)
        }
    }

    fun showFilterPopup(view: View) {
        if (popupFilter == null) { //Init first time click to detect anchor view
            popupFilter = PopupMenu(requireContext(), view)
            popupFilter!!.inflate(R.menu.menu_filter_home)
            popupFilter!!.menu.forEach {
                it.isCheckable = true
                it.isChecked = true //Default is show all devices
            }
            popupFilter?.setOnMenuItemClickListener { item: MenuItem ->
                item.isChecked = !item.isChecked
                item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW)
                item.actionView = View(requireContext())
                when (item.itemId) {
                    R.id.filter_light -> editFilterCondition(item.isChecked, DeviceType.LIGHT)
                    R.id.filter_heater -> editFilterCondition(item.isChecked, DeviceType.HEATER)
                    R.id.filter_roller -> editFilterCondition(item.isChecked, DeviceType.ROLLER)
                }
                mBinding.adapter?.filter?.filter(null)
                false
            }
        }
        popupFilter!!.show()
    }

    fun onProfileClick(view: View) {
        shareViewModel.userData.postValue(userProfile.value)
        navigator.navigateTo(Screens.PROFILE)
    }

    override fun onItemClick(device: Device) {
        shareViewModel.deviceData.postValue(device)
        appDatabase.getDevice(device.id, device.productType) { stateDevice ->
            shareViewModel.deviceData.postValue(stateDevice) //If there is local data -> Update current device
        }
        navigator.navigateTo(Screens.LEVEL)
    }

    override fun onItemDeleteClick(position: Int, device: Device) {
        ConfirmDialog(requireContext(), getString(R.string.confirm_delete_device)) {
            listDevices.remove(device)
            mBinding.adapter?.items?.remove(device)
            mBinding.adapter?.notifyItemRemoved(position)
            mBinding.adapter?.notifyItemRangeChanged(position, mBinding.adapter?.items?.size ?: 0)
        }
    }

    override fun onItemChangeMode(device: Device, isOn: Boolean) {
        val mode = if (isOn) DeviceMode.ON.type else DeviceMode.OFF.type
        when (device) {
            is Light -> device.mode = mode
            is Heater -> device.mode = mode
        }
        appDatabase.insertDevice(device) {}
    }

    fun onLanguageChangeEn(view: View) {
        ConfirmDialog(requireContext(), getString(R.string.confirm_change_to_eng)) {
            runBlocking { dataStoreUtil.saveLanguage(Language.EN) }
            reloadActivity(Language.EN.language)
        }
    }

    fun onLanguageChangeFr(view: View) {
        ConfirmDialog(requireContext(), getString(R.string.confirm_change_to_fr)) {
            runBlocking { dataStoreUtil.saveLanguage(Language.FR) }
            reloadActivity(Language.FR.language)
        }
    }

    private fun reloadActivity(languageCode: String) {
        val locale = Locale(languageCode)
        Locale.setDefault(locale)
        val resources: Resources = resources
        val config: Configuration = resources.configuration
        config.setLocale(locale)
        resources.updateConfiguration(config, resources.displayMetrics)
        val activity = requireActivity()
        activity.finish()
        activity.startActivity(activity.intent)
    }

}