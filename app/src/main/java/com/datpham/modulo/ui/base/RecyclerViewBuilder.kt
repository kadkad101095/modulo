package com.datpham.modulo.ui.base
import androidx.databinding.ViewDataBinding

/*
* Neu muon dung view type thi lam nhu sau
*     val testAdapter = RecyclerViewBuilder()
*         .addRow(R.layout.item_post, postRecyclerViewViewModel, 0)
*         .addRow(R.layout.item_second, SecondAdapterViewModel(), 1) //Neu co viewtype thi add them ViewModel va type nhu nay
*         .build(posts)
* */
class RecyclerViewBuilder {
    private var rowInfos: ArrayList<BaseRecyclerViewRow> = ArrayList()

    fun addRow(layoutId: Int, viewModel: MyObservable): RecyclerViewBuilder {
        val infor = BaseRecyclerViewRow(layoutId, viewModel)
        rowInfos.add(infor)
        return this
    }

    fun addRow(layoutId: Int, viewModel: MyObservable, viewType: Int): RecyclerViewBuilder {
        val infor = BaseRecyclerViewRow(layoutId, viewModel, viewType)
        rowInfos.add(infor)
        return this
    }

    fun <M> build(items: ArrayList<M>): BaseRecyclerViewAdapter<M, ViewDataBinding> {
        val adapter = BaseRecyclerViewAdapter<M, ViewDataBinding>()
        adapter.rows = rowInfos
        adapter.items = items
        return adapter
    }

}

class BaseRecyclerViewRow(
    var layoutId: Int = 0,
    var viewModel: MyObservable? = null,
    var viewType: Int = 0
)