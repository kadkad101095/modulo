package com.datpham.modulo.ui.base

import androidx.databinding.BaseObservable
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel

abstract class MyObservable : ViewModel() { //ViewModel cho adapter
    abstract fun newInstance(): MyObservable
    abstract fun <MODEL> bind(model: MODEL, position: Int)
    var binding: ViewDataBinding? = null
}