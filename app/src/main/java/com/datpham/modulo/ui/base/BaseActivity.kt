package com.datpham.modulo.ui.base

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.datpham.modulo.R
import com.datpham.modulo.ui.main.view.dialog.ConfirmDialog
import com.datpham.modulo.ui.main.view.dialog.LoadingDialog
import com.datpham.modulo.ui.main.view.dialog.NotyDialog
import com.datpham.modulo.utils.TAG


abstract class BaseActivity<VIEWDATABINDING : ViewDataBinding, VIEWMODEL : ViewModel> :
    AppCompatActivity() {

    lateinit var mBinding: VIEWDATABINDING
    lateinit var mViewModel: VIEWMODEL
    lateinit var loadingDialog: LoadingDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, getLayout())
        mBinding.lifecycleOwner = this
        loadingDialog = LoadingDialog(this)
        mViewModel = ViewModelProvider(this).get(getViewModel())
        setupUI()
        setupObserver()
    }

    override fun onDestroy() {
        super.onDestroy()
        mBinding.unbind()
    }

    protected abstract fun getLayout(): Int

    protected abstract fun getViewModel(): Class<VIEWMODEL>

    protected abstract fun setupUI()

    protected abstract fun setupObserver()

    open fun openFragment(fragmentId: Int, passData: Bundle? = null) {
        findNavController(R.id.nav_host_fragment).navigate(fragmentId, passData)
    }

    fun showLoading() {
        loadingDialog.show()
    }

    fun hideLoading() {
        loadingDialog.dismiss()
    }

    fun showToast(message: String, success: Boolean = false) {
        loadingDialog.dismiss()
        NotyDialog(this, message, success)
    }

    fun showToast(messageId: Int, success: Boolean = false) {
        loadingDialog.dismiss()
        NotyDialog(this, getString(messageId), success)
    }

    fun showLogE(message: String) {
        Log.e(TAG, message)
    }

    fun showLogD(message: String) {
        Log.d(TAG, message)
    }

    override fun onBackPressed() {
        if (findNavController(R.id.nav_host_fragment).graph.startDestination == findNavController(R.id.nav_host_fragment).currentDestination?.id) {
            ConfirmDialog(this, getString(R.string.confirm_exit_application)) {
                finish()
            }
        } else {
            super.onBackPressed()
        }
    }

}
