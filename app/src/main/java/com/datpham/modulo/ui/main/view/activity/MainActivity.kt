package com.datpham.modulo.ui.main.view.activity

import android.content.Context
import android.content.ContextWrapper
import com.datpham.modulo.R
import com.datpham.modulo.data.model.Language
import com.datpham.modulo.databinding.ActivityMainBinding
import com.datpham.modulo.ui.base.BaseActivity
import com.datpham.modulo.ui.main.viewmodel.MainViewModel
import com.datpham.modulo.utils.ContextUtils
import com.datpham.modulo.utils.DataStoreUtil
import com.datpham.modulo.utils.PreferencesKeys
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    @Inject
    lateinit var dataStoreUtil: DataStoreUtil

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun setupUI() {
        //Language
        val preference = runBlocking { dataStoreUtil.dataStore.data.first() }
        val lang = preference[PreferencesKeys.LANGUAGE] ?: Language.EN.language
        //TODO: Update language
    }

    override fun setupObserver() {

    }

}