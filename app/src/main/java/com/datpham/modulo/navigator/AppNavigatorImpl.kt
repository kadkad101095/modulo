package com.datpham.modulo.navigator

import androidx.fragment.app.FragmentActivity
import com.datpham.modulo.R
import com.datpham.modulo.ui.base.BaseActivity
import javax.inject.Inject

class AppNavigatorImpl @Inject constructor(private val activity: FragmentActivity) : AppNavigator {

    override fun navigateTo(screen: Screens) {
        val fragmentId = when (screen) {
            Screens.HOME -> R.id.homeFragment
            Screens.PROFILE -> R.id.action_homeFragment_to_profileFragment
            Screens.LEVEL -> R.id.action_homeFragment_to_levelFragment
        }
        (activity as BaseActivity<*, *>).openFragment(fragmentId)
    }

}
