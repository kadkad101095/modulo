package com.datpham.modulo.navigator

/**
 * Available screens.
 */
enum class Screens {
    HOME,
    PROFILE,
    LEVEL
}

/**
 * Interfaces that defines an app navigator.
 */
interface AppNavigator {
    // Navigate to a given screen.
    fun navigateTo(screen: Screens)
}
