package com.datpham.modulo.data.repository.remote.exceptions

import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String get() = "No Internet Connection"
}