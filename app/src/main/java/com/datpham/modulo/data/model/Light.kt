package com.datpham.modulo.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.datpham.modulo.data.model.Device

@Entity
data class Light(
    @PrimaryKey override val id: Int,
    override val deviceName: String,
    override val productType: String,
    var intensity: Int,
    var mode: String,
) : Device()
