package com.datpham.modulo.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RollerShutter(
    @PrimaryKey override val id: Int,
    override val deviceName: String,
    override val productType: String,
    var position: Int
) : Device()
