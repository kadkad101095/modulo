package com.datpham.modulo.data.model

enum class Language(val language: String) {
    EN("en"),
    FR("fr")
}