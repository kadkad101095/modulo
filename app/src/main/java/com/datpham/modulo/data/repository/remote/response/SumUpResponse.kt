package com.datpham.modulo.data.repository.remote.response

import com.datpham.modulo.data.model.Device
import com.datpham.modulo.data.model.User

data class SumUpResponse(val devices: ArrayList<Device>, val user: User)
