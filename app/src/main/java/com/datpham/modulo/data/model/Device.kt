package com.datpham.modulo.data.model

abstract class Device {
    abstract val id: Int
    abstract val deviceName: String
    abstract val productType: String
}
