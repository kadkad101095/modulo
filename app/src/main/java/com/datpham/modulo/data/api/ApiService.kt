package com.datpham.modulo.data.api

import com.datpham.modulo.data.repository.remote.response.SumUpResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface ApiService {

    @GET("data.json")
    fun getData(): Observable<SumUpResponse>

}