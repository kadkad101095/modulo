package com.datpham.modulo.data.repository.local

import android.annotation.SuppressLint
import android.util.Log
import androidx.room.Database
import androidx.room.RoomDatabase
import com.datpham.modulo.data.model.DeviceType
import com.datpham.modulo.data.model.Device
import com.datpham.modulo.data.model.Heater
import com.datpham.modulo.data.model.Light
import com.datpham.modulo.data.model.RollerShutter
import com.datpham.modulo.utils.TAG
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

@SuppressLint("CheckResult")
@Database(entities = [Light::class, Heater::class, RollerShutter::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun deviceDao(): DeviceDao

    private val errorHandler = Consumer<Throwable?> {
        Log.e(TAG, it.message ?: "ERROR")
    }

    fun getAllDevices(success: Consumer<List<Device>>) {
        Observable.zip(
            deviceDao().getAllLight().observeOn(Schedulers.io()),
            deviceDao().getAllHeater().observeOn(Schedulers.io()),
            deviceDao().getAllRollerShutter().observeOn(Schedulers.io()),
            { lights, heaters, rollers ->
                val listDevices = ArrayList<Device>()
                listDevices.addAll(lights)
                listDevices.addAll(heaters)
                listDevices.addAll(rollers)
                listDevices
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getDevice(id: Int, type: String, success: Consumer<Device>) {
        when (type) {
            DeviceType.LIGHT.type -> getLight(id, Consumer {
                success.accept(it)
            })
            DeviceType.HEATER.type -> getHeater(id, Consumer {
                success.accept(it)
            })
            DeviceType.ROLLER.type -> getRollerShutter(id, Consumer {
                success.accept(it)
            })
        }
    }

    private fun getLight(id: Int, success: Consumer<Light>) {
        deviceDao().getLight(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getHeater(id: Int, success: Consumer<Heater>) {
        deviceDao().getHeater(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun getRollerShutter(id: Int, success: Consumer<RollerShutter>) {
        deviceDao().getRollerShutter(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(success, errorHandler)
    }

    fun insertDevice(device: Device, success: Action) {
        when (device) {
            is Light -> {
                deviceDao().insertLights(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, errorHandler)
            }
            is Heater -> {
                deviceDao().insertHeaters(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, errorHandler)
            }
            is RollerShutter -> {
                deviceDao().insertRollerShutters(device)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success, errorHandler)
            }
        }
    }
}