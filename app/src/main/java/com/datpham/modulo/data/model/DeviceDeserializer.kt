package com.datpham.modulo.data.model

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type


class DeviceDeserializer : JsonDeserializer<Device> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Device? {
        val obj = json?.asJsonObject
        return when (obj?.get("productType")?.asString) {
            DeviceType.LIGHT.type -> context?.deserialize(obj, Light::class.java)
            DeviceType.HEATER.type -> context?.deserialize(obj, Heater::class.java)
            else -> context?.deserialize(obj, RollerShutter::class.java)
        }
    }
}