package com.datpham.modulo.data.api

import android.content.Context
import com.datpham.modulo.data.repository.remote.exceptions.NetworkConnectionInterceptor
import com.datpham.modulo.data.repository.remote.exceptions.NoConnectivityException
import com.datpham.modulo.R
import com.datpham.modulo.data.model.Device
import com.datpham.modulo.data.model.DeviceDeserializer
import com.datpham.modulo.data.repository.remote.response.SumUpResponse
import com.datpham.modulo.utils.BASE_URL
import com.datpham.modulo.utils.Resource
import com.google.gson.GsonBuilder
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiManager @Inject constructor(@ApplicationContext val context: Context) {

    var apiService: ApiService
    private val compositeDisposable = CompositeDisposable()

    init {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(NetworkConnectionInterceptor(context))
            .build()
        val gson = GsonBuilder()
            .registerTypeAdapter(Device::class.java, DeviceDeserializer())
            .create()

        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    fun getData(success: Consumer<Resource<SumUpResponse>>) {
        success.accept(Resource.loading())
        compositeDisposable.add(
            apiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    success.accept(Resource.success(it))
                }, onError(success))
        )
    }

    private fun <T> onError(success: Consumer<Resource<T>>): Consumer<Throwable?> {
        return Consumer<Throwable?> { it ->
            success.accept(
                Resource.error(
                    when (it) {
                        is NoConnectivityException -> context.getString(R.string.network_error)
                        is HttpException -> context.getString(R.string.error)
                        else -> context.getString(R.string.error)
                    }
                )
            )
        }
    }

    fun clear() {
        compositeDisposable.dispose()
    }

}