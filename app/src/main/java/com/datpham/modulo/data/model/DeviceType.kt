package com.datpham.modulo.data.model

enum class DeviceType(val type: String) {
    LIGHT("Light"),
    HEATER("Heater"),
    ROLLER("RollerShutter")
}