package com.datpham.modulo.data.model

enum class DeviceMode(val type: String) {
    ON("ON"),
    OFF("OFF")
}