package com.datpham.modulo.utils

const val BASE_URL: String = "http://storage42.com/modulotest/"

const val TAG: String = "KAD MODULO"
const val DATABASE_NAME = "MODULOTEST"

//HEATER Temperature
const val HEATER_MAX_TEMP = 28
const val HEATER_MIN_TEMP = 7
const val HEATER_STEP = 0.5