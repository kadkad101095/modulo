package com.datpham.modulo.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}