package com.datpham.modulo.utils

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.datpham.modulo.data.model.Language
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

private const val PREFERENCES_NAME = "MODULO"

object PreferencesKeys {
    val LANGUAGE = stringPreferencesKey("language")
}

private val Context.dataStore by preferencesDataStore(PREFERENCES_NAME)

class DataStoreUtil @Inject constructor(@ApplicationContext context: Context) {
    val dataStore = context.dataStore
    suspend fun saveLanguage(lang: Language) {
        dataStore.edit {
            it[PreferencesKeys.LANGUAGE] = lang.language
        }
    }

}