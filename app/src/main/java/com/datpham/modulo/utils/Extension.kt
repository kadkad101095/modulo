package com.datpham.modulo.utils

import android.util.Patterns
import com.datpham.modulo.ui.base.BaseActivity
import com.datpham.modulo.ui.base.BaseFragment
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun Long.formatDate(): String {
    val date = Date(this)
    return date.toString("dd/MM/yyyy")
}

fun String.isValidDate(format: String): Boolean {
    return try {
        val df: DateFormat = SimpleDateFormat(format, Locale.getDefault())
        df.setLenient(false)
        df.parse(this)
        true
    } catch (e: ParseException) {
        false
    }
}

fun String.isValidEmail(): Boolean {
    if (this == "") return false
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPhone(): Boolean {
    if (this == "" || this.length < 9 || this.length > 11) return false
    return Patterns.PHONE.matcher(this).matches()
}

fun String.toDate(format: String): Date? {
    val sdf = SimpleDateFormat(format, Locale.US)
    return sdf.parse(this)
}

fun Date.toString(format: String): String {
    return SimpleDateFormat(format, Locale.getDefault()).format(this)
}

fun BaseFragment<*, *>.showLoading() {
    (requireActivity() as BaseActivity<*, *>).showLoading()
}

fun BaseFragment<*, *>.hideLoading() {
    (requireActivity() as BaseActivity<*, *>).hideLoading()
}

fun BaseFragment<*, *>.showToast(message: String, isSuccess: Boolean = false) {
    (requireActivity() as BaseActivity<*, *>).showToast(message, isSuccess)
}

fun BaseFragment<*, *>.showToast(message: Int, isSuccess: Boolean = false) {
    (requireActivity() as BaseActivity<*, *>).showToast(message, isSuccess)
}