package com.datpham.modulo.resource

import android.view.View
import androidx.test.espresso.IdlingResource

class ViewVisibilityIdlingResource(view: View, expectedVisibility: Int) :
    IdlingResource {
    private val mView: View = view
    private val mExpectedVisibility: Int = expectedVisibility
    private var mIdle: Boolean
    private var mResourceCallback: IdlingResource.ResourceCallback?
    override fun getName(): String {
        return ViewVisibilityIdlingResource::class.java.simpleName
    }

    override fun isIdleNow(): Boolean {
        mIdle = mIdle || mView.visibility == mExpectedVisibility
        if (mIdle) {
            if (mResourceCallback != null) {
                mResourceCallback!!.onTransitionToIdle()
            }
        }
        return mIdle
    }

    override fun registerIdleTransitionCallback(resourceCallback: IdlingResource.ResourceCallback) {
        mResourceCallback = resourceCallback
    }

    init {
        mIdle = false
        mResourceCallback = null
    }

}