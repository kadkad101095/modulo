package com.datpham.modulo.matcher

import android.view.View
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

class ViewSizeMatcher(private val expectedWith: Int, private val expectedHeight: Int) :
    TypeSafeMatcher<View?>(View::class.java) {

    override fun describeTo(description: Description) {
        description.appendText("with SizeMatcher: ")
        description.appendValue(expectedWith.toString() + "x" + expectedHeight)
    }

    override fun matchesSafely(target: View?): Boolean {
        val targetWidth: Int = target?.width ?: 0
        val targetHeight: Int = target?.height ?: 0
        return targetWidth == expectedWith && targetHeight == expectedHeight
    }

}