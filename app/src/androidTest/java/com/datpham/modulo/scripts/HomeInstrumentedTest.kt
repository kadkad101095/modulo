package com.datpham.modulo.scripts

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.uiautomator.UiDevice
import com.datpham.modulo.R
import com.datpham.modulo.matcher.BaseMatcher
import com.datpham.modulo.matcher.ViewSizeMatcher
import com.datpham.modulo.ui.main.view.activity.MainActivity
import com.datpham.modulo.utils.SetSeekArcValueAction
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Before
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class HomeInstrumentedTest {

    val context = InstrumentationRegistry.getInstrumentation().targetContext
    val baseMatcher = BaseMatcher()

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var testRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        hiltRule.inject()
    }

    @Test
    fun checkDefaultDisplay() {
        //Wait fragment to loaded
        baseMatcher.waifForWithId(R.id.imgFrame)
        //Check avatar size
        val avatarSize = context.resources.getDimensionPixelSize(R.dimen._42sdp)
        onView(withId(R.id.imgFrame))
            .check(matches(ViewSizeMatcher(avatarSize, avatarSize)))
        //Check text color
        onView(withId(R.id.tvWelcome))
            .check(matches(baseMatcher.textViewTextColorMatcher(Color.parseColor("#b6b4b4"))))
        //Check recyclerview size
        onView(withId(R.id.rcvDevices)).check(matches(baseMatcher.recyclerViewSizeMatcher(12)))
        //Check first recyclerview item show right text
        onView(withId(R.id.rcvDevices)).check(
            matches(
                baseMatcher.recyclerViewAtPositionOnView(
                    0,
                    withText("Lampe - Cuisine"),
                    R.id.tvName
                )
            )
        )
    }

    @Test
    fun checkAvatar() {
        //Wait fragment to loaded
        baseMatcher.waifForWithId(R.id.imgFrame)
        //Click image
        onView(withId(R.id.imgFrame)).perform(click())
        //Wait fragment to loaded
        baseMatcher.waifForWithId(R.id.btnConfirm)
        //Click confirm
        onView(withId(R.id.btnConfirm)).perform(click())
    }

    @Test
    fun checkUsername() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgFrame)
        //Check username show correctly
        //Duplicate id tvName -> must use allOf and not decendant to get right view
        onView(allOf(withId(R.id.tvName), not(isDescendantOfA(withId(R.id.rcvDevices))))).check(
            matches(withText("John Doe"))
        )
        //Change text to a long text to check lines
        onView(allOf(withId(R.id.tvName), not(isDescendantOfA(withId(R.id.rcvDevices)))))
            .perform(baseMatcher.setTextInTextView("ABCDEFGHIKLMNOPL"))
        //Check username text still one line
        onView(allOf(withId(R.id.tvName), not(isDescendantOfA(withId(R.id.rcvDevices)))))
            .check(matches(baseMatcher.isTextInLines(1)))
    }

    @Test
    fun checkLanguague() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgFrame)
        //Change languague to France
        onView(withId(R.id.btnLanguagueFrance)).perform(click())
        //Wait dialog show up
        baseMatcher.waifForWithId(R.id.btnConfirm)
        //Click confirm
        onView(withId(R.id.btnConfirm)).perform(click())
        //Wait fragment reload
        baseMatcher.waifForWithId(R.id.imgFrame)
        //Check isFrench language
        onView(withId(R.id.tvWelcome))
            .check(matches(withText("Bienvenue à la maison!")))
        //Change back to English
        onView(withId(R.id.btnLanguagueEngland)).perform(click())
        //Wait dialog show up
        baseMatcher.waifForWithId(R.id.btnConfirm)
        //Click confirm
        onView(withId(R.id.btnConfirm)).perform(click())
    }

    @Test
    fun checkFilter() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.btnFilter)
        //Open filter dialog
        onView(withId(R.id.btnFilter)).perform(click())
        //Click filter out heater and roller
        onView(withText("Heater")).inRoot(baseMatcher.isPopupWindow()).perform(click())
        onView(withText("RollerShutter")).inRoot(baseMatcher.isPopupWindow()).perform(click())
        //Close popup menu
        onView(isRoot()).perform(pressBack())
        //Check list has light only
        onView(withId(R.id.rcvDevices)).check(
            matches(
                baseMatcher.recyclerViewAtPositionOnView(
                    0,
                    baseMatcher.withDrawable(R.mipmap.ic_light),
                    R.id.imgIcon
                )
            )
        )
        onView(withId(R.id.rcvDevices)).check(
            matches(
                baseMatcher.recyclerViewAtPositionOnView(
                    1,
                    baseMatcher.withDrawable(R.mipmap.ic_light),
                    R.id.imgIcon
                )
            )
        )
        onView(withId(R.id.rcvDevices)).check(
            matches(
                baseMatcher.recyclerViewAtPositionOnView(
                    2,
                    baseMatcher.withDrawable(R.mipmap.ic_light),
                    R.id.imgIcon
                )
            )
        )
    }

    @Test
    fun lightDeviceDisplay() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgIcon)
        //Turn on off
        onView(withId(R.id.rcvDevices)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                baseMatcher.clickChildViewWithId(R.id.btnSwitch)
            )
        )
        //Open light
        onView(withId(R.id.rcvDevices)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                baseMatcher.clickChildViewWithId(R.id.layoutItemHome)
            )
        )
        //Wait fragment open
        baseMatcher.waifForWithId(R.id.icTypeOverlay)
        //Check icon type light
        onView(withId(R.id.icTypeOverlay)).check(matches(baseMatcher.withDrawable(R.mipmap.ic_light)))
        //Check text color
        onView(withId(R.id.tvLevel))
            .check(matches(baseMatcher.textViewTextColorMatcher(Color.parseColor("#4e4e4e"))))
    }

    @Test
    fun lightTestValue() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgIcon)
        //Turn on off
        onView(withId(R.id.rcvDevices)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                baseMatcher.clickChildViewWithId(R.id.layoutItemHome)
            )
        )
        //Wait fragment open
        baseMatcher.waifForWithId(R.id.icTypeOverlay)
        //Set light value
        onView(withId(R.id.viewLevel)).perform(SetSeekArcValueAction(10))
        //Go back
        onView(isRoot()).perform(pressBack())
        //Check value correct
        onView(withId(R.id.rcvDevices)).check(
            matches(
                baseMatcher.recyclerViewAtPositionOnView(
                    0,
                    withText("10%"),
                    R.id.tvPercent
                )
            )
        )
    }

    @Test
    fun lightTestDelete() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgIcon)
        //Click delete
        onView(withId(R.id.rcvDevices)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                baseMatcher.clickChildViewWithId(R.id.btnDelete)
            )
        )
        //Wait dialog show up
        baseMatcher.waifForWithId(R.id.btnClose)
        //Click close
        onView(withId(R.id.btnClose)).perform(click())
        //Check recyclerview size the same
        onView(withId(R.id.rcvDevices)).check(matches(baseMatcher.recyclerViewSizeMatcher(12)))
        //Click delete again
        onView(withId(R.id.rcvDevices)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                baseMatcher.clickChildViewWithId(R.id.btnDelete)
            )
        )
        //Wait dialog show up
        baseMatcher.waifForWithId(R.id.btnConfirm)
        //Click confirm thí time
        onView(withId(R.id.btnConfirm)).perform(click())
        //Check recyclerview size decrease 1
        onView(withId(R.id.rcvDevices)).check(matches(baseMatcher.recyclerViewSizeMatcher(11)))
    }

    @Test
    fun testScrollUpDown() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgIcon)
        //Scroll down
        onView(withId(R.id.rcvDevices)).perform(swipeUp())
        //Check last item visible
        onView(withId(R.id.rcvDevices)).check(
            matches(
                baseMatcher.recyclerViewAtPositionOnView(
                    11,
                    isDisplayed(),
                    R.id.tvPercent
                )
            )
        )
        //Scroll up
        onView(withId(R.id.rcvDevices)).perform(swipeDown())
    }

    @Test
    fun testOpenrecentApp() {
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        uiDevice.pressRecentApps()
        Thread.sleep(1000)
        uiDevice.pressRecentApps()
        Thread.sleep(1000)
        uiDevice.pressRecentApps()
        Thread.sleep(1000)
        uiDevice.pressRecentApps()
        Thread.sleep(1000)
    }

    @Test
    fun testClickHomeApp() {
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        uiDevice.pressHome()
        Thread.sleep(1000)
        uiDevice.pressRecentApps()
        Thread.sleep(1000)
        uiDevice.pressRecentApps()
        Thread.sleep(1000)
    }

    @Test
    fun testClickBack() {
        //Wait home fragment loaded
        baseMatcher.waifForWithId(R.id.imgFrame)
        onView(isRoot()).perform(pressBack())
        //Wait fragment to loaded
        baseMatcher.waifForWithId(R.id.btnConfirm)
        //Click confirm
        onView(withId(R.id.btnConfirm)).perform(click())
        Thread.sleep(1000)
    }
}