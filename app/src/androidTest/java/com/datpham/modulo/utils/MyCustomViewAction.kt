package com.datpham.modulo.utils

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import com.datpham.modulo.ui.customviews.MySeekArc
import org.hamcrest.Matcher


class SetSeekArcValueAction(val value: Int) : ViewAction {
    override fun getConstraints(): Matcher<View> {
        return isAssignableFrom(MySeekArc::class.java)
    }

    override fun getDescription(): String {
        return "whatever"
    }

    override fun perform(uiController: UiController?, view: View) {
        val yourCustomView: MySeekArc = view as MySeekArc
        yourCustomView.progress = value
        // tadaaa
    }
}